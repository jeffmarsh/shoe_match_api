'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*
 Modules make it possible to import JavaScript files into your application.  Modules are imported
 using 'require' statements that give you a reference to the module.

  It is a good idea to list the modules that your application depends on in the package.json in the project root
 */
var util = require('util');

/*
 Once you 'require' a module you can reference the things that it exports.  These are defined in module.exports.

 For a controller in a127 (which this is) you should export the functions referenced in your Swagger document by name.

 Either:
  - The HTTP Verb of the corresponding operation (get, put, post, delete, etc)
  - Or the operationId associated with the operation in your Swagger document

  In the starter/skeleton project the 'get' operation on the '/hello' path has an operationId named 'hello'.  Here,
  we specify that in the exports of this module that 'hello' maps to the function named 'hello'
 */
module.exports = {
  getShoesFromShoe: getShoesFromShoe,
  getShoeModelNames: getShoeModelNames,
  getShoeData: getShoeData

};

/*
  Functions in a127 controllers used for operations should take two parameters:

  Param 1: a handle to the request object
  Param 2: a handle to the response object
 */

function getShoeModelNames(req, res) {
  // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
  var brand = req.swagger.params.manufacturer.value || 'stranger';
  var model = req.swagger.params.model.value || 'stranger';
  var models = ["Air", "Air Zoom", "Air Zoom Pegasus", "Air Zoom Pegasus", "Air Zoom Elite", "Air Zoom Structure", "Air Zoom Fit"];
  //var reply = util.format('manufacturer name, %s, model, %s!', brand, model);
  var replyJSON = {
    "manufacturer" : brand,
    "models" : models
  };

  // this sends back a JSON response which is a single string
  res.json(replyJSON);
}

function getShoeData(req, res){
  var replyJSON = {
    "picture": "whatever.jpg",
    "brand": "Nike",
    "name": "Free 5.0 Flash",
    "shoe_id":  "685168-7.0-D",
    "length": "250.00000",
    "toe_box": "242.00000",
    "instep": "51.00000",
    "width": "83.00000",
    "mid_foot": "70.00000",
    "heel": "54.00000"
  };
  res.json(replyJSON);
}

function getShoesFromShoe(req, res){
  var replyJSON = [
    { "picture": "whatever_1.jpg", "brand": "Nike", "name": "Free 5.0 Flash1", "shoe_id":  "685168-7.0-D-1", "length": "250.00000", "toe_box": "242.00000", "instep": "51.00000", "width": "83.00000", "mid_foot": "70.00000", "heel": "54.00000"},
    { "picture": "whatever_2.jpg", "brand": "Nike", "name": "Free 5.0 Flash2", "shoe_id":  "685168-7.0-D-2", "length": "210.00000", "toe_box": "242.00000", "instep": "51.00000", "width": "83.00000", "mid_foot": "70.00000", "heel": "54.00000"},
    { "picture": "whatever_3.jpg", "brand": "Nike", "name": "Free 5.0 Flash3", "shoe_id":  "685168-7.0-D-3", "length": "2504.00000", "toe_box": "242.00000", "instep": "51.00000", "width": "83.00000", "mid_foot": "70.00000", "heel": "54.00000"},
    { "picture": "whatever_4.jpg", "brand": "Nike", "name": "Free 5.0 Flash4", "shoe_id":  "685168-7.0-D-4", "length": "255.00000", "toe_box": "242.00000", "instep": "51.00000", "width": "83.00000", "mid_foot": "70.00000", "heel": "54.00000"}
  ];
  res.json(replyJSON);
}
